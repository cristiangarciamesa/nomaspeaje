UtilApp = {

	ocultarPanel : function() {
		// create menu variables
		var slideoutMenu = $('.slideout-menu');
		var slideoutMenuWidth = $('.slideout-menu').width();
		var darkPanel = $('.darkPanel');
		// toggle open class
		slideoutMenu.toggleClass("open");
		darkPanel.toggleClass("oculto");

		slideoutMenu.animate({
			left : -slideoutMenuWidth
		}, 250);
	},
	
	viewMessage : function(message) {
		if (message != null) {
			Session.set(Constant.SESSION_MESSAGE, message);
			$("#message").modal("show");
		}
	}

}