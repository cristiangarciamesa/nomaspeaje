Template.menuItems.events({
	
	'click .inicio': function(){
		UtilApp.ocultarPanel();
  		Bender.go('/', {}, {animation : 'slideLeft'});
  	},

	'click .mensaje-legal': function(){
		UtilApp.ocultarPanel();
  		Bender.go('/mensajeLegal', {}, {animation : 'slideLeft'});
  	},

  	'click .apoyo': function(){
		UtilApp.ocultarPanel();
  		Bender.go('/apoyo', {}, {animation : 'slideLeft'});
  	},

  	'click .base-legal-simple': function(){
		UtilApp.ocultarPanel();
  		Bender.go('/baseLegalSimple', {}, {animation : 'slideLeft'});
  	},


	'click .base-legal': function(){
		UtilApp.ocultarPanel();
  		Bender.go('/baseLegal/', {}, {animation : 'slideLeft'});
  	}

});