Router.configure({
	layoutTemplate : 'template',
	notFoundTemplate : 'notFound',
});

Router.route('/', {
	name : 'index'
});

Router.route('/mensajeLegal', function() {
	this.render('mensajeLegal', {});
});

Router.route('/apoyo', function() {
	this.render('apoyo', {});
});

Router.route('/baseLegalSimple', function() {
	this.render('baseLegalSimple', {});
});

Router.route('/baseLegal', function() {
	this.render('baseLegal', {});
});