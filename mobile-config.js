App.info({
	id: 'es.dairagua.nomaspeaje',
  	name: 'nomaspeaje',
  	description: 'Aplicación informativa para evitar el pago de peajes.',
  	author: 'Grupo Dairagua',
  	email: 'grupodairagua@gmail.com',
  	version: '1.0.0'
});

//https://docs.meteor.com/api/mobile-config.html#App-icons
App.icons({
  	'android_mdpi': 'public/icons/nmp.png',
  	'android_hdpi': 'public/icons/nmp.png',
	'android_xhdpi': 'public/icons/nmp.png',
	'android_xxhdpi': 'public/icons/nmp.png',  
	'android_xxxhdpi': 'public/icons/nmp.png'
});

//https://docs.meteor.com/api/mobile-config.html#App-launchScreens
App.launchScreens({
	'android_mdpi_portrait': 'public/icons/nmp.png',
	'android_mdpi_landscape': 'public/icons/nmp.png',
	'android_hdpi_portrait': 'public/icons/nmp.png',
	'android_hdpi_landscape': 'public/icons/nmp.png',
	'android_xhdpi_portrait': 'public/icons/nmp.png',
	'android_xhdpi_landscape': 'public/icons/nmp.png',
	'android_xxhdpi_portrait': 'public/icons/nmp.png',
	'android_xxhdpi_landscape': 'public/icons/nmp.png'
});

App.setPreference('AllowInlineMediaPlayback', true);


App.accessRule("*");